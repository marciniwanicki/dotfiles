
```
  __          __       ___      ___                    
 /\ \        /\ \__  /'___\ __ /\_ \                   
 \_\ \    ___\ \ ,_\/\ \__//\_\\//\ \      __    ____  
 /'_` \  / __`\ \ \/\ \ ,__\/\ \ \ \ \   /'__`\ /',__\ 
/\ \L\ \/\ \L\ \ \ \_\ \ \_/\ \ \ \_\ \_/\  __//\__, `\
\ \___,_\ \____/\ \__\\ \_\  \ \_\/\____\ \____\/\____/
 \/__,_ /\/___/  \/__/ \/_/   \/_/\/____/\/____/\/___/ 

 #aliases #functions #macos-setup #iterm2
```

The repository contains a mix of popular open sourced dotfiles shared
on github as well as my own random setup.
**To be clear, I do not recommend you to fork that project :-)** First, there are much
better and well maintained dotfiles out there, second, you might end up
installing lots of stuff on you machine that will never be used. It's probably
better if you spend some time reading the files and crafting new ones
fitting best your own needs. I bet you will do it well and even learn some
new shell tricks.

## The real .files ninjas, thanks to all of them
- Mathias Bynens - https://github.com/mathiasbynens/dotfiles (most of .aliases
  and brew.sh lines are coming from his repository)

## How to install

```bash
sh -c "$(curl -fsSL https://bitbucket.org/marciniwanicki/dotfiles/raw/master/bin/install.sh)"
```

## License
All files in this repository are released under the MIT license. [See LICENSE](https://github.com/marciniwanicki/dotfiles/blob/develop/LICENSE) for details.
